package com.Matthew;

import com.Matthew.domain.BerlinClock;
import org.junit.Test;

import java.time.LocalDateTime;
import java.time.Month;

import static org.junit.Assert.*;

/**
 * Created by matthew2chambers on 24/08/2016.
 */
public class BerlinClockTest {

    @Test
    public void WHEN_current_time_is_000000_THEN_all_4_lamp_blocks_should_have_0_activated() {

        // Exercise
        BerlinClock clockTime_000000 = new BerlinClock(LocalDateTime.of(2016, Month.JANUARY, 1, 00, 00, 00));

        // Verify

        assertEquals(0, clockTime_000000.getActivatedFiveHourBlockCount());
        assertEquals(0, clockTime_000000.getActivatedSingleHourBlockCount());
        assertEquals(0, clockTime_000000.getActivatedFiveMinuteBlockCount());
        assertEquals(0, clockTime_000000.getActivatedSingleMinuteBlockCount());
    }

    @Test
    public void WHEN_current_time_is_10AM_THEN_2_of_the_5_hour_lamps_should_be_activated() {

        // Exercise
        BerlinClock clockTime = new BerlinClock(LocalDateTime.of(2016, Month.JANUARY, 1, 10, 00, 00));

        // Verify
        assertEquals(2, clockTime.getActivatedFiveHourBlockCount());
    }

    @Test
    public void WHEN_current_time_is_1PM_THEN_3_of_the_1_hour_lamps_should_be_activated() {

        // Exercise
        BerlinClock clockTime = new BerlinClock(LocalDateTime.of(2016, Month.JANUARY, 1, 13, 00, 00));

        // Verify
        assertEquals(3, clockTime.getActivatedSingleHourBlockCount());

    }

    @Test
    public void WHEN_current_time_is_39_minutes_into_an_hour_THEN_7_of_the_5_minute_lamps_and_4_of_the_1_minute_lamps_should_be_activated() {

        // Exercise
        BerlinClock clockTime = new BerlinClock(LocalDateTime.of(2016, Month.JANUARY, 1, 00, 39, 00));

        // Verify
        assertEquals(7, clockTime.getActivatedFiveMinuteBlockCount());
        assertEquals(4, clockTime.getActivatedSingleMinuteBlockCount());

    }

    @Test
    public void WHEN_current_time_is_50_minutes_into_an_hour_THEN_10_of_the_5_minute_lamps_and_0_of_the_1_minute_lamps_should_be_activated() {

        // Exercise
        BerlinClock clockTime = new BerlinClock(LocalDateTime.of(2016, Month.JANUARY, 1, 00, 50, 00));

        // Verify
        assertEquals(10, clockTime.getActivatedFiveMinuteBlockCount());
        assertEquals(0, clockTime.getActivatedSingleMinuteBlockCount());

    }

    @Test
    public void WHEN_current_time_unit_of_secounds_is_an_even_number_THEN_state_flag_should_be_on() throws InterruptedException {

        // Setup

        // Exercise
        BerlinClock clockTime_000000 = new BerlinClock(LocalDateTime.of(2016, Month.JANUARY, 1, 00, 00, 00));
        BerlinClock clockTime = new BerlinClock(LocalDateTime.of(2016, Month.JANUARY, 1, 10, 39, 46));
        BerlinClock clockTime_230003 = new BerlinClock(LocalDateTime.of(2016, Month.JANUARY, 1, 23, 00, 04));

        // Verify

        assertTrue(clockTime_000000.isSecondState());
        assertTrue(clockTime.isSecondState());
        assertTrue(clockTime_230003.isSecondState());
    }

    @Test
    public void WHEN_current_time_unit_of_secounds_is_and_odd_number_THEN_state_flag_should_be_off() throws InterruptedException {

        // Setup

        // Exercise
        BerlinClock clockTime_235959 = new BerlinClock(LocalDateTime.of(2016, Month.JANUARY, 1, 23, 59, 59));
        BerlinClock clockTime = new BerlinClock(LocalDateTime.of(2016, Month.JANUARY, 1, 10, 39, 47));
        BerlinClock clockTime_230003 = new BerlinClock(LocalDateTime.of(2016, Month.JANUARY, 1, 23, 00, 03));

        // Verify

        assertFalse(clockTime_235959.isSecondState());
        assertFalse(clockTime.isSecondState());
        assertFalse(clockTime_230003.isSecondState());
    }

}
