package com.Matthew.api.exception;

/**
 * Created by matthew2chambers on 06/10/2016.
 */

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

public class DataInputExceptionMatcher extends TypeSafeMatcher<DataInputException> {

    public static DataInputExceptionMatcher hasError(DataInputException.ERROR_TYPE errorType) {
        return new DataInputExceptionMatcher(errorType);
    }

    private DataInputException.ERROR_TYPE expectedErrorType;
    private DataInputException.ERROR_TYPE actualErrorType;

    private DataInputExceptionMatcher(DataInputException.ERROR_TYPE errorType) {
        this.expectedErrorType = errorType;
    }

    @Override
    public boolean matchesSafely(final DataInputException exception) {
        this.actualErrorType = exception.getErrorType();
        return this.actualErrorType.equals(this.expectedErrorType);
    }

    @Override
    public void describeTo(Description description) {
        description.appendValue(actualErrorType)
                .appendText(" were found instead of ")
                .appendValue(expectedErrorType);
    }

}