package com.Matthew;

import com.Matthew.api.InputParser;
import com.Matthew.api.exception.DataInputException;
import com.Matthew.api.exception.DataInputExceptionMatcher;
import com.Matthew.api.implementations.InputParserImplementation;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

/**
 * Created by matthew2chambers on 06/10/2016.
 */
public class InputParserTest {

    InputParser inputParser = new InputParserImplementation();


    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void WHEN_input_time_is_empty_THEN_an_exception_should_be_thrown() {

        // Pre-verify registration
        thrown.expect(DataInputException.class);
        thrown.expect(DataInputExceptionMatcher.hasError(DataInputException.ERROR_TYPE.NOT_ALL_TIME_SEGMENTS_FOUND));

        inputParser.parseTime("");
    }

    @Test
    public void WHEN_input_time_contains_non_numeric_characters_THEN_an_exception_should_be_thrown() {

        // Pre-verify registration
        thrown.expect(DataInputException.class);
        thrown.expect(DataInputExceptionMatcher.hasError(DataInputException.ERROR_TYPE.TIME_VALUES_NOT_NUMERIC));

        inputParser.parseTime("10:a:55");
    }

    @Test
    public void WHEN_input_time_contains_numbers_but_no_delimeters_THEN_an_exception_should_be_thrown() {

        // Pre-verify registration
        thrown.expect(DataInputException.class);
        thrown.expect(DataInputExceptionMatcher.hasError(DataInputException.ERROR_TYPE.NOT_ALL_TIME_SEGMENTS_FOUND));

        // Exercise
        inputParser.parseTime("10");

    }

    @Test
    public void WHEN_input_time_contains_numbers_but_only_1_delimeter_THEN_an_exception_should_be_thrown() {

        // Pre-verify registration
        thrown.expect(DataInputException.class);
        thrown.expect(DataInputExceptionMatcher.hasError(DataInputException.ERROR_TYPE.NOT_ALL_TIME_SEGMENTS_FOUND));

        // Exercise
        inputParser.parseTime("10:45");
    }

    @Test
    public void WHEN_input_time_contains_invalid_high_hour_value_THEN_an_exception_should_be_thrown() {

        // Pre-verify registration
        thrown.expect(DataInputException.class);
        thrown.expect(DataInputExceptionMatcher.hasError(DataInputException.ERROR_TYPE.HOURS_INVALID));

        // Exercise
        inputParser.parseTime("25:00:00");

    }

    @Test
    public void WHEN_input_time_contains_invalid_low_hour_value_THEN_an_exception_should_be_thrown() {

        // Pre-verify registration
        thrown.expect(DataInputException.class);
        thrown.expect(DataInputExceptionMatcher.hasError(DataInputException.ERROR_TYPE.HOURS_INVALID));

        // Exercise
        inputParser.parseTime("-3:00:00");

    }

    @Test
    public void WHEN_input_time_contains_invalid_high_minute_value_THEN_an_exception_should_be_thrown() {

        // Pre-verify registration
        thrown.expect(DataInputException.class);
        thrown.expect(DataInputExceptionMatcher.hasError(DataInputException.ERROR_TYPE.MINUTES_INVALID));

        // Exercise
        inputParser.parseTime("10:62:00");

    }

    @Test
    public void WHEN_input_time_contains_invalid_low_minute_value_THEN_an_exception_should_be_thrown() {

        // Pre-verify registration
        thrown.expect(DataInputException.class);
        thrown.expect(DataInputExceptionMatcher.hasError(DataInputException.ERROR_TYPE.MINUTES_INVALID));

        // Exercise
        inputParser.parseTime("10:-2:00");

    }

    @Test
    public void WHEN_input_time_contains_invalid_high_seconds_value_THEN_an_exception_should_be_thrown() {

        // Pre-verify registration
        thrown.expect(DataInputException.class);
        thrown.expect(DataInputExceptionMatcher.hasError(DataInputException.ERROR_TYPE.SECONDS_INVALID));

        // Exercise
        inputParser.parseTime("10:00:123456");

    }

    @Test
    public void WHEN_input_time_contains_invalid_low_seconds_value_THEN_an_exception_should_be_thrown() {

        // Pre-verify registration
        thrown.expect(DataInputException.class);
        thrown.expect(DataInputExceptionMatcher.hasError(DataInputException.ERROR_TYPE.SECONDS_INVALID));

        // Exercise
        inputParser.parseTime("10:00:-2");

    }
}
