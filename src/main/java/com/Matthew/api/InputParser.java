package com.Matthew.api;

import java.time.LocalDateTime;

/**
 * Created by matthew2chambers on 06/10/2016.
 */
public interface InputParser {
    public LocalDateTime parseTime(String input);
}
