package com.Matthew.api;

import com.Matthew.domain.BerlinClock;

/**
 * Created by matthew2chambers on 06/10/2016.
 */
public interface BerlinClockPrinter {
    public void printTime(BerlinClock clock);
}