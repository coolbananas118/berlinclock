package com.Matthew.api.exception;

/**
 * Created by matthew2chambers on 06/10/2016.
 */
public class DataInputException extends RuntimeException {

    private ERROR_TYPE errorType;

    public DataInputException(ERROR_TYPE errorType, String input) {
        this.errorType = errorType;
    }

    public ERROR_TYPE getErrorType() {
        return errorType;
    }

    public enum ERROR_TYPE {
        TIME_VALUES_NOT_NUMERIC, HOURS_INVALID, MINUTES_INVALID, NOT_ALL_TIME_SEGMENTS_FOUND, SECONDS_INVALID;
    }
}
