package com.Matthew.api.implementations;

import com.Matthew.api.InputParser;
import com.Matthew.api.exception.DataInputException;

import java.time.LocalDateTime;
import java.time.Month;

/**
 * Created by matthew2chambers on 06/10/2016.
 */
public class InputParserImplementation implements InputParser {

    @Override
    public LocalDateTime parseTime(String input) {

        Integer hours;
        Integer minutes;
        Integer seconds;

        String[] timeAsStringArray = input.split(":");

        if (timeAsStringArray.length != 3) {
            throw new DataInputException(DataInputException.ERROR_TYPE.NOT_ALL_TIME_SEGMENTS_FOUND, input);
        }

        try {
            hours = Integer.parseInt(timeAsStringArray[0]);
            minutes = Integer.parseInt(timeAsStringArray[1]);
            seconds = Integer.parseInt(timeAsStringArray[2]);
        } catch (NumberFormatException e) {
            throw new DataInputException(DataInputException.ERROR_TYPE.TIME_VALUES_NOT_NUMERIC, input);
        }

        if (hours < 0 || hours > 23) {
            throw new DataInputException(DataInputException.ERROR_TYPE.HOURS_INVALID, hours.toString());
        } else if (minutes < 0 || minutes > 59) {
            throw new DataInputException(DataInputException.ERROR_TYPE.MINUTES_INVALID, minutes.toString());
        } else if (seconds < 0 || seconds > 59) {
            throw new DataInputException(DataInputException.ERROR_TYPE.SECONDS_INVALID, seconds.toString());
        }

        return LocalDateTime.of(2016, Month.JANUARY, 1, hours, minutes, seconds);
    }
}
