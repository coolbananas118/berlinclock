package com.Matthew.api.implementations;

import com.Matthew.api.BerlinClockPrinter;
import com.Matthew.domain.BerlinClock;
import com.Matthew.domain.BerlinClockColor;

import java.util.stream.IntStream;

/**
 * Created by matthew2chambers on 25/09/2016.
 */
public class BerlinClockConsolePrinter implements BerlinClockPrinter {

    @Override
    public void printTime(BerlinClock clock) {

        if (clock.isSecondState()) {
            System.out.println("------------[ON]------------");
        } else {
            System.out.println("------------[OFF]-----------");
        }
        printFourSectionBlock(clock.getActivatedFiveHourBlockCount(), BerlinClockColor.RED);
        printFourSectionBlock(clock.getActivatedSingleHourBlockCount(), BerlinClockColor.RED);
        printElevenSectionBlock(clock.getActivatedFiveMinuteBlockCount());
        printFourSectionBlock(clock.getActivatedSingleMinuteBlockCount(), BerlinClockColor.YELLOW);

    }

    private void printFourSectionBlock(int activatedCount, BerlinClockColor color) {

        StringBuilder blockAsString = new StringBuilder();
        if (color == BerlinClockColor.RED) {
            IntStream.rangeClosed(1, activatedCount).forEach(i ->
                    blockAsString.append("  RED .")
            );
        } else {
            IntStream.rangeClosed(1, activatedCount).forEach(i ->
                    blockAsString.append(color).append(".")
            );
        }
        IntStream.rangeClosed(activatedCount + 1, 4).forEach(i -> blockAsString.append("      ."));
        appendActivedCountString(activatedCount, blockAsString);

        System.out.println(blockAsString.toString());

    }

    private void printElevenSectionBlock(int activatedCount) {

        StringBuilder blockAsString = new StringBuilder();

        for (int i = 1; i <= 11; i++) {
            if (i <= activatedCount) {
                if (i % 3 == 0) {
                    blockAsString.append(" R.");
                } else {
                    blockAsString.append(" Y");
                }
            } else {
                if (i % 3 == 0) {
                    blockAsString.append("  .");
                } else {
                    blockAsString.append("  ");
                }
            }
        }

        blockAsString.append("  .");
        appendActivedCountString(activatedCount, blockAsString);

        System.out.println(blockAsString.toString());

    }

    private void appendActivedCountString(int activatedCount, StringBuilder blockAsString) {
        blockAsString.append(" (");
        blockAsString.append(activatedCount);
        blockAsString.append(")");

    }
}
