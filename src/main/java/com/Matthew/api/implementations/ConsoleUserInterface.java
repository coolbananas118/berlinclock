package com.Matthew.api.implementations;

import com.Matthew.api.BerlinClockPrinter;
import com.Matthew.api.InputParser;
import com.Matthew.api.UserInterface;
import com.Matthew.api.exception.DataInputException;
import com.Matthew.domain.BerlinClock;

import java.time.LocalDateTime;
import java.util.*;

/**
 * Created by matthew2chambers on 06/10/2016.
 */
public class ConsoleUserInterface implements UserInterface {

    private InputParser inputParser;
    private Scanner scanner;
    BerlinClockPrinter printer;

    public ConsoleUserInterface(InputParser inputParser, Scanner scanner, BerlinClockPrinter printer) {
        this.inputParser = inputParser;
        this.scanner = scanner;
        this.printer = printer;
    }

    public void start() {
        showWelcomeMessage();
        readConsoleInput();
    }

    private void readConsoleInput() {

        String line = null;

        while (true) {

            if ((line = scanner.nextLine()) != null) {
                try {
                    if (line.equals("EXIT")) {
                        break;
                    }

                    LocalDateTime time = inputParser.parseTime(line);

                    BerlinClock berlinClock = new BerlinClock(time);

                    printer.printTime(berlinClock);

                } catch (DataInputException | NumberFormatException e) {
                    System.out.println("Incorrect time format, please try again.");
                } catch (Exception e) {
                    System.out.println("Error processing input, please try again.");
                }
            }
        }
    }

    private void showWelcomeMessage() {
        System.out.println("Welcome to a console berlin clock application.");
        System.out.println("\nEnter EXIT to stop the application or a time in the following format to see it printed in Berlin Clock style: hours:minutes:seconds (hours should be 24-based) e.g. 14:57:05)");
    }

}
