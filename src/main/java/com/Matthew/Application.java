package com.Matthew;

import com.Matthew.api.UserInterface;
import com.Matthew.api.implementations.BerlinClockConsolePrinter;
import com.Matthew.api.implementations.InputParserImplementation;
import com.Matthew.api.implementations.ConsoleUserInterface;

import java.util.Scanner;

/**
 * Created by matthew2chambers on 25/09/2016.
 */
public class Application {
    public static void main(String[] args) throws InterruptedException {

        System.out.println("--------------------------------------------");
        System.out.println("Starting Berlin Clock console application...");
        System.out.println("--------------------------------------------");

        UserInterface userInterface = new ConsoleUserInterface(new InputParserImplementation(), new Scanner(System.in), new BerlinClockConsolePrinter());
        userInterface.start();

    }
}
