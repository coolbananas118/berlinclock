package com.Matthew.domain;

import java.time.LocalDateTime;

/**
 * Created by matthew2chambers on 25/09/2016.
 */
final public class BerlinClock {

    private final LocalDateTime clockTime;
    private final int activatedFiveHourBlockCount;
    private final int activatedSingleHourBlockCount;
    private final int activatedFiveMinuteBlockCount;
    private final int activatedSingleMinuteBlockCount;

    private final boolean secondState;

    public BerlinClock(LocalDateTime clockTime) {
        this.clockTime = clockTime;

        this.activatedSingleHourBlockCount = clockTime.getHour() % 5;
        this.activatedFiveHourBlockCount = (clockTime.getHour() - activatedSingleHourBlockCount) / 5;

        this.activatedSingleMinuteBlockCount = clockTime.getMinute() % 5;
        this.activatedFiveMinuteBlockCount = (clockTime.getMinute() - activatedSingleMinuteBlockCount) / 5;

        if (clockTime.getSecond() % 2 == 0) {
            secondState = true;
        } else {
            secondState = false;
        }
    }

    public LocalDateTime getClockTime() {
        return clockTime;
    }

    public int getActivatedSingleHourBlockCount() {
        return activatedSingleHourBlockCount;
    }

    public int getActivatedFiveHourBlockCount() {
        return activatedFiveHourBlockCount;
    }

    public int getActivatedFiveMinuteBlockCount() {
        return activatedFiveMinuteBlockCount;
    }

    public int getActivatedSingleMinuteBlockCount() {
        return activatedSingleMinuteBlockCount;
    }

    public boolean isSecondState() {
        return secondState;
    }
}
