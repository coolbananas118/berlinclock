# Berlin Clock console application

This project is a console application which returns user input time in Berlin Clock visual format (https://en.wikipedia.org/wiki/Mengenlehreuhr).

It requires Maven and a Java 8 JDK installed.

Steps to run:
mvn clean install
java -jar target/BerlinClock-0.0.1-SNAPSHOT.jar

TODO: Add unit test coverage of BerlinClockConsolePrinter and ConsoleUserInterface.